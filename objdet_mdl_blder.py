import cv2
import numpy as np
import pickle
from img_manager import ImgManager
from os import listdir
from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn import metrics

# Configure HOG detector
win_size = (112, 128)
cell_size = (4, 4)
nbins = 9
block_size = (8, 8)
block_stride = (8, 8)
hog = cv2.HOGDescriptor(win_size, block_size, block_stride, cell_size, nbins)

# Read training images
train_features = []
train_labels = []
img_dir = 'train_imgs_hog'
im = ImgManager()
for img_file in listdir(img_dir):
    img = im.preprocess_image_from_file(img_dir, img_file, size=win_size)
    train_features.append(hog.compute(img))
    train_labels.append(int(img_file.split('_')[0]))

# Split dataset into training set and test set
# Note: Must enable shuffle for training to be successful! 
X_train, X_test, y_train, y_test = train_test_split(train_features, train_labels, test_size= 0.01, shuffle=True)

# Create a svm classifier
clf = svm.SVC(kernel='linear')

# Train the model using the training sets
clf.fit(X_train, y_train)

# Save model to disk
pickle.dump(clf, open('svm.bin', 'wb'))

# Load model from disk
# clf = pickle.load(open('svm.bin', 'rb'))

# Predict the response for test dataset
y_pred = clf.predict(X_test)

# Model accuracy
print("Accuracy: ", metrics.accuracy_score(y_test, y_pred))

# Load SVM vectors from disk
# vec = pickle.load(open('vec.bin', 'rb'))

# Configure detector
vec = np.array(clf.coef_)
rho = clf.decision_function(X_train)[0]
vec = np.append(vec, -rho)
hog.setSVMDetector(vec)

# Save SVM vectors to disk
pickle.dump(vec, open('vec.bin', 'wb'))

# # Create linear SVM
# svm = cv2.ml.SVM_create()
# svm.setKernel(cv2.ml.SVM_LINEAR)

# # Train and save SVM
# svm.train(np.array(train_features), cv2.ml.ROW_SAMPLE, np.array(train_labels))
# svm.save('svm.xml')

# # Configure detector
# vec = svm.getSupportVectors()
# rho, _, _ = svm.getDecisionFunction(0)
# vec = np.append(vec, -rho)
# hog.setSVMDetector(vec)

# Load test images and classify
img_dir = 'test_imgs_hog'
for img_file in listdir(img_dir):
    img = im.preprocess_image_from_file(img_dir, img_file)
    res = hog.detectMultiScale(img)

    if (len(res[1]) > 0 and np.max(res[1]) > 0.1):
        index = np.argmax(res[1])
        (x, y, w, h) = res[0][index]
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
        text = 'confidence = {}'.format(np.max(res[1]))
        cv2.putText(img, text, (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0))
    im.display_single_image('Img', img)
