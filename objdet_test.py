import cv2
import numpy as np
import pickle
from cam_manager import CamManager
from img_manager import ImgManager
from os import listdir
from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn import metrics

# Load trained SVM model and vectors from file
filename = 'svm.bin'
clf = pickle.load(open('svm.bin', 'rb'))
vec = pickle.load(open('vec.bin', 'rb'))

# Configure HOG detector
win_size = (112, 128)
cell_size = (4, 4)
nbins = 9
block_size = (8, 8)
block_stride = (8, 8)
hog = cv2.HOGDescriptor(win_size, block_size, block_stride, cell_size, nbins)

# Configure detector
hog.setSVMDetector(vec)

# Initialise camera
cm = CamManager()
cm.init_camera()

# Initialise image manager
im = ImgManager()

# Read frames continuously from camera until
# Esc key is pressed
conf_buffer = np.zeros(10)
while True:
    # Read frame from camera
    frame = cm.read_frame()
    frame = im.preprocess_image(frame)

    # Detect presence of subject(s)
    res = hog.detectMultiScale(frame)

    # Draw a rectangle around subject if found and confidence is high enough
    if (len(res[1]) > 0 and np.max(res[1]) > 0.15):
        index = np.argmax(res[1])
        (x, y, w, h) = res[0][index]
        cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)

    # Draw text to show confidence level
    if len(res[1]) > 0:
        conf_buffer = np.roll(conf_buffer, 1)
        conf_buffer[0] = np.max(res[1])
        text = 'confidence = {}'.format(np.average(conf_buffer))
        cv2.putText(frame, text, (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0))
    
    # Show camera feed
    cv2.imshow('Live Feed', frame)

    # Wait for key press and timeout after 1ms
    c = cv2.waitKey(1)
    if c == 27:
        break

# Destroy all image windows
cv2.destroyAllWindows()