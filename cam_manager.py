import cv2

class CamManager:
    def __init__(self):
        self.frame = None
        self.stream = None

    def init_camera(self):
        """
        Initialise the first camera the system can find
        """
        self.stream = cv2.VideoCapture(0)
        # Check if the webcam is opened correctly
        if not self.stream.isOpened():
            raise IOError("Cannot open webcam")

    def read_frame(self, size=None):
        """
        Read a frame from camera
        :param size:    Tuple of width and height to resize frame. Leave as
                        None to disable resizing. 
        :return:        Frame image if frame is read successfully, else None. 
        """
        ret, frame = self.stream.read()
        if size:
            frame = cv2.resize(frame, size, interpolation=cv2.INTER_AREA)
        if not ret:
            raise IOError("Failed to read frame from camera")
        return frame

    def destroy_camera(self):
        """
        Destroys camera's instance and release its resource. 
        """
        self.stream.release()
