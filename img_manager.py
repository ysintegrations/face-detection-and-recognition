import cv2
import numpy as np


class ImgManager:
    def __init__(self):
        self.img = None

    def display_single_image(self, name, image):
        """
        Displays image in a window. 
        Waits for any keypress to close the window. 

        :param name:    Name of the window
        :param image:   Image to be displayed
        """
        cv2.imshow(name, image)
        cv2.waitKey()
        cv2.destroyAllWindows()

    def preprocess_image_from_file(self, img_dir, img_file, size=None, norm=True, astype='float64', expand_dim=False):
        """
        Center and standardise input image from file. 

        :param img_dir:     Directory of input image
        :param img_file:    File name of input image
        :param size:        Tuple containing width and height of image after
                            resizing. If size is None, image will not be 
                            resized. 
        :return:            Processed image
        """
        img = cv2.imread(img_dir + '/' + img_file, cv2.IMREAD_COLOR)
        if size:
            img = cv2.resize(img, size)
        if astype == 'float64':
            img = img.astype(np.float64) - np.mean(img)
        elif astype == 'float32':
            img = img.astype(np.float32) - np.mean(img)
        else:
            pass
        img /= np.std(img)
        if expand_dim:
            img = np.expand_dims(img, axis=0)
        if norm:
            img = cv2.normalize(img, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
        return img

    def preprocess_image(self, image, size=None, norm=True, astype='float64', expand_dim=False):
        """
        Center and standardise input image. 

        :param image:       Directory of input image
        :param size:        Tuple containing width and height of image after
                            resizing. If size is None, image will not be 
                            resized. 
        :return:            Processed image
        """
        if size:
            img = cv2.resize(image, size)
        else:
            img = image
        if astype == 'float64':
            img = img.astype(np.float64) - np.mean(img)
        elif astype == 'float32':
            img = img.astype(np.float32) - np.mean(img)
        else:
            pass
        img /= np.std(img)
        if expand_dim:
            img = np.expand_dims(img, axis=0)
        if norm:
            img = cv2.normalize(img, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
        return img