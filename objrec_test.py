from os import listdir
from cam_manager import CamManager
from img_manager import ImgManager
from PIL import Image
import tflite_runtime.interpreter as tflite
import numpy as np
import cv2
import pickle

# Initialise camera
cm = CamManager()
cm.init_camera()

# Create interpreter
ip = tflite.Interpreter(model_path='object_recognition.tflite')
ip.allocate_tensors()

# Initialise image manager
im = ImgManager()

# Load trained SVM model and vectors from file
filename = 'svm.bin'
clf = pickle.load(open('svm.bin', 'rb'))
vec = pickle.load(open('vec.bin', 'rb'))

# Configure HOG detector
win_size = (112, 128)
cell_size = (4, 4)
nbins = 9
block_size = (8, 8)
block_stride = (8, 8)
hog = cv2.HOGDescriptor(win_size, block_size, block_stride, cell_size, nbins)

# Configure detector
hog.setSVMDetector(vec)

# Get input/output indices
input_index = ip.get_input_details()[0]['index']
output_index = ip.get_output_details()[0]['index']

# Read frames continuously from camera until
# Esc key is pressed
buffer = np.zeros(10)
while True:
    # Read frame from camera
    raw_frame = cm.read_frame()
    cframe = raw_frame
    frame = im.preprocess_image(raw_frame)

    # Detect presence of subject(s)
    res = hog.detectMultiScale(frame)

    # Draw a rectangle around subject if found and confidence is high enough
    if (len(res[1]) > 0 and np.max(res[1]) > 0):
        index = np.argmax(res[1])
        (x, y, w, h) = res[0][index]
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
    
        # Crop frame
        cframe = raw_frame[y:(y + h), x:(x + w)]
        # Image.open(raw_frame).crop((x, y, x + w, y + h))

        # Send image to interpreter
        img = im.preprocess_image(cframe, size=(112, 128), astype='float32', norm=False, expand_dim=True)
        ip.set_tensor(input_index, img)

        # Launch interpreter and get prediction
        ip.invoke()
        preds = ip.get_tensor(output_index)
        pred = np.argmax(preds)
        if pred == 1:
            subject = 'DS is wearing face mask'
        elif pred == 2:
            subject = 'DS is not wearing face mask'
        else:
            subject = 'Found a face, not sure who is this'

    # Draw text to show confidence level
    if len(res[1]) > 0:
        buffer = np.roll(buffer, 1)
        buffer[0] = np.max(preds)
        msg = '{}, {:.2f}%'.format(subject, np.average(buffer) * 100)
        cv2.putText(frame, msg, (x, y + h + 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0))
    else:
        subject = 'Found a face, not sure who is this'
        msg = '{}'.format(subject)
        cv2.putText(frame, msg, (320, 200), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0))

    # Show camera feed
    cv2.imshow('Live Feed', frame)

    # Wait for key press and timeout after 1ms
    c = cv2.waitKey(1)
    if c == 27:
        break

# Destroy all image windows
cv2.destroyAllWindows()
